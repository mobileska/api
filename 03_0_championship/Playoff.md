# Описание методов API, связанных со матчами плей-офф

## Загрузка результатов серий игр плей-офф в сезоне

### URL

`GET championship/{id}/gamesseries`

### Ответ

```json
[
    {
        "title":"Кубок Гагарина",
        "stage":
        {
            "title":"Финал",
            "series":
            [
                {
                    "homeTeam":
                    {
                        "id":"123",
                        "name":"Название команды, которая играет дома",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "guestsTeam":
                    {
                        "id":"124",
                        "name":"Название команды, которая играет в гостях",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "score":
                    {
                        "homeTeam":
                        {
                            "goals":3
                        },
                        "guestsTeam":
                        {
                            "goals":0
                        }
                    }
                }
            ]
        }
    },
    {
        "title":"Западная конференция",
        "stage":
        {
            "title":"1/2 финала",
            "series":
            [
                {
                    "homeTeam":
                    {
                        "id":"123",
                        "name":"Название команды, которая играет дома",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "guestsTeam":
                    {
                        "id":"124",
                        "name":"Название команды, которая играет в гостях",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "score":
                    {
                        "homeTeam":
                        {
                            "goals":3
                        },
                        "guestsTeam":
                        {
                            "goals":0
                        }
                    }
                },
                {
                    "homeTeam":
                    {
                        "id":"123",
                        "name":"Название команды, которая играет дома",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "guestsTeam":
                    {
                        "id":"124",
                        "name":"Название команды, которая играет в гостях",
                        "abbreviation":"АБР",
                        "city":"Название родного города команды",
                        "country":"Название родной страны команды",
                        "logoUrl":"http://img.com"
                    },
                    "score":
                    {
                        "homeTeam":
                        {
                            "goals":3
                        },
                        "guestsTeam":
                        {
                            "goals":0
                        }
                    }
                }
            ]
        }
    }
]
```

## Загрузка матчей в серии игр плей-офф в сезоне

### URL

`GET championship/{id}/gamesseries?homeTeamId={id}&guestsTeamId={id}`

### Ответ

```json
[
    {
        "id":"12321",
        "date":"YYYY-MM-DDThh:mm:ss",
        "tournamentName":"РЕГУЛЯРНЫЙ ЧЕМПИОНАТ",
        "stadiumName":"ВТБ ЛЕДОВЫЙ ДВОРЕЦ",
        "status":1,
        "homeTeam":
        {
            "id":"123",
            "name":"Название команды, которая играет дома",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logoUrl":"http://img.com"
        },
        "guestsTeam":
        {
            "id":"124",
            "name":"Название команды, которая играет в гостях",
            "abbreviation":"АБР",
            "city":"Название родного города команды",
            "country":"Название родной страны команды",
            "logoUrl":"http://img.com"
        },
        "score":
        {
            "homeTeam":
            {
                "goals":3,
                "events":
                [
                    {
                        "type":0,
                        "title":"",
                        "time":"11:58",
                        "players": [ "Сергей Калинин", "Широков Сергей", "Хафизуллин Динар" ]
                    },
                    {
                        "type":1,
                        "title":"Задержка клюшкой!",
                        "time":"12:43",
                        "players": [ "Артём Зуб" ]
                    }
                ]
            },
            "guestsTeam":
            {
                "goals":0,
                "events":null
            }
        },
        "isOurTeamHomeMatch":true
    }
]
```

`status` – статус матча, принимает значения:

- 0 – матч до начала (анонс)
- 1 – матч во время игры (активен)
- 2 – матч после завершения (не активен)

`events`-> `type` – тип события, принимает значения:

- 0 – гол
- 1 – удаление