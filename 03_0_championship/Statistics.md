# Описание методов API, связанных со статистикой команды в чемпионате

## Загрузка статистики в чемпионате

### URL

`GET championship/{id}/stats`

### Ответ

```json
[
    {
        "id":"0",
        "categoryName":"Место",
        "parameters":
        [
            {
                "id":"1",
                "title":"Регулярный чемпионат",
                "value":"1"
            }
        ]
    },
    {
        "id":"1",
        "categoryName":"Игры",
        "parameters":
        [
            {
                "id":"1",
                "title":"Количество игр",
                "value":"23"
            }
        ]
    }
]
```

## Загрузка результатов игр

### URL

`GET championship/{id}/stats/games`

### Ответ

```json
[
    {
        "date":"YYYY-MM-DDThh:mm:ss",
        "opposingTeam":
        {
            "name":"Название команды",
            "logoUrl":"http://img.com"
        },
        "score":
        {
            "homeTeam":
            {
                "goals":3
            },
            "guestsTeam":
            {
                "goals":0
            }
        },
        "isOurTeamHomeMatch":true
    }
]
```