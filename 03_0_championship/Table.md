# Описание методов API, связанных с турнирной таблицей чемпионата

## Загрузка фильтров для отображения таблицы

### URL

`GET championship/filters`

### Ответ

```json
[
    {
        "id":"1",
        "title":"Регулярный чемпионат"
    },
    {
        "id":"1",
        "title":"Западный дивизион"
    }
]
```

## Загрузка полей турнирной таблицы

### URL

`GET championship/{id}/table`

`id` – идентификатор, полученный из `championship/filters`

### Ответ

```json
[
    {
        "logoUrl":"http://some_logo_domain.com/team1.png",
        "position":1,
        "teamName":"СКА",
        "matchesCount":31,
        "points":81,
        "highlightedTeam":true
    },
    {
        "logoUrl":"http://some_logo_domain.com/team2.png",
        "position":2,
        "teamName":"ЙОКЕРИТ",
        "matchesCount":27,
        "points":61
    }
]
```