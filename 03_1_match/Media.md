# Описание методов АПИ, связанных с загрузкой медиаматериалов о матче

## Загрузка фото-новостей

### URL

`GET game/{id}/media/photo`

### Ответ

```json
[
    {
        "id":"photoNews1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```

## Загрузка видео-новостей

### URL

`GET game/{id}/media/video`

### Ответ

```json
[
    {
        "id":"videoNews1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```