# Описание методов АПИ, связанных с новостями о матче

## Загрузка новостей

### URL

`GET game/{id}/news`

### Ответ

```json
[
    {
        "id":"news1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```