# Описание методов АПИ, связанных со статистикой матча

## Загрузка информации о лучшем игроке матча

### URL

`GET game/{id}/bestplayer`

### Ответ

```json
{
    "parameter":"27% сейвов",
    "player":
    {
        "id":"1",
        "photoUrl":"http:/image.com",
        "fullName":"Игорь Шестеркин",
        "age":21,
        "number":19,
        "roleName":"Вратарь"
    }
}
```

## Загрузка статистики матча

### URL

`GET game/{id}/statistics`

### Ответ

```json
[
    {
        "id":"0",
        "categoryName":"Статистика матча",
        "parameters":
        [
            {
                "id":"1",
                "title":"Броски",
                "homeTeam":
                {
                    "key":"СКА",
                    "value":"33"
                },
                "guestsTeam":
                {
                    "key":"Спартак",
                    "value":"30"
                }
            },
            {
                "id":"2",
                "title":"Отраженные броски",
                "homeTeam":
                {
                    "key":"СКА",
                    "value":"33"
                },
                "guestsTeam":
                {
                    "key":"Спартак",
                    "value":"30"
                }
            }
        ]
    }
]
```