# Описание методов АПИ, связанных с турнирной таблицей по чемпионату, в рамках которого проводится матч

## Загрузка турнирной таблицы

### URL

`GET game/{id}/table`

### Ответ

```json
[
    {
        "position":1,
        "teamName":"СКА",
        "matchesCount":31,
        "points":81
    },
    {
        "position":2,
        "teamName":"ЙОКЕРИТ",
        "matchesCount":27,
        "points":61
    }
]
```