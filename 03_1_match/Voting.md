# Описание методов АПИ, связанных с голосованием по итогам матча

## Получение признака активности голосования

### URL

`GET game/{id}/voting/activity`

### Ответ

```json
{
    "isActive":true
}
```

## Получение списка игроков для голосования

### URL

`GET game/{id}/voting/players`

### Ответ

```json
[
    {
        "id":"1",
        "fullName":"Патрик Херсли"
    }
]
```

## Отправка результата голосования

### URL

`GET game/{id}/voting/players?playerId={id}`

### Ответ

`200 OK`

## Получение результатов голосования

### URL

`GET game/{id}/voting/results`

### Ответ

```json
[
    {
        "votes":"153 голоса",
        "player":
        {
            "id":"1",
            "photoUrl":"http:/image.com",
            "fullName":"Игорь Шестеркин",
            "age":21,
            "number":19,
            "roleName":"Вратарь"
        }
    },
    {
        "votes":"56 голосов",
        "player":
        {
            "id":"2",
            "photoUrl":"http:/image.com",
            "fullName":"Игорь Шестеркин",
            "age":21,
            "number":19,
            "roleName":"Вратарь"
        }
    },
    {
        "votes":"26 голосов",
        "player":
        {
            "id":"3",
            "photoUrl":"http:/image.com",
            "fullName":"Игорь Шестеркин",
            "age":21,
            "number":19,
            "roleName":"Вратарь"
        }
    }
]
```