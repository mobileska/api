# Описание методов АПИ, связанных со списком игроков

## Загрузка списка игроков

### URL

#### Все игроки

`GET team/players`

#### В определенном чемпионате

`GET team/players?champId={id}`

#### С определенной ролью в команде

`GET team/players?rolesId={id}`

#### С определенной ролью в команде и в конкретном чемпионате

`GET team/players?champId={id}&rolesId={id}`

#### Фильтрация игроков

```json
{
    "champId":"1",
    "rolesId":"1"
}
```

### Ответ

```json
[
    {
        "groupName":"Вратари",
        "players":
        [
            {
                "id":"1",
                "photoUrl":"http:/image.com",
                "fullName":"Игорь Шестеркин",
                "age":21,
                "number":19,
                "roleName":"Вратарь"
            }
        ]
    }
]
```

## Загрузка фильтров для списка игроков

### URL

`GET team/players/filters`

### Ответ

```json
{
    "championships":
    [
        {
            "id":"1",
            "title":"2017/2018"
        }
    ],
    "roles":
    [
        {
            "id":"0",
            "title":"Все"
        },
        {
            "id":"1",
            "title":"Вратари"
        }
    ]
}
```