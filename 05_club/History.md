# Описание методов АПИ, связанных с историей клуба

## Загрузка списка периодов

### URL

`GET club/history/periods`

### Ответ

```json
[
    {
        "id":"11",
        "title":"1960-e"
    },
    {
        "id":"12",
        "title":"1970-e"
    }
]
```

## Загрузка списка событий в периоде

### URL

`GET club/history?periodId={id}`

### Ответ

```json
[
    {
        "id":"1",
        "iconUrl":"http://icon.com/1",
        "year":"1968",
        "title":"В кубковом финале"
    }
]
```

## Загрузка подробного описания события

### URL

`GET club/history/{id}/`

### Ответ

```json
{
    "text":"html с простым текстовым форматированием без картинок"
}
```