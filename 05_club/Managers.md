# Описание методов АПИ, связанных с руководством клуба

## Загрузка списка менеджеров

### URL

`GET club/persons?type=0`

### Ответ

```json
[
    {
        "id":"1",
        "photoUrl":"http://photo.com",
        "fullName":"Геннадий Николаевич Тимченко",
        "roleName":"Президент"
    }
]
```