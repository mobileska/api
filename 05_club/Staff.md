# Описание методов АПИ, связанных с персоналом клуба

## Загрузка списка менеджеров

### URL

`GET club/persons?type=2`

### Ответ

```json
[
    {
        "id":"1",
        "photoUrl":"http://photo.com",
        "fullName":"Геннадий Николаевич Тимченко",
        "roleName":"Президент"
    }
]
```