# Описание методов АПИ, связанных Insgagram

## Загрузка списка фотографий

### URL

`GET media/instagram`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "photoUrl":"http://photo.com/1600x900"
    }
]
```