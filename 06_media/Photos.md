# Описание методов АПИ, связанных с фотографиями

## Загрузка подборки фотографий на главной

### URL

`GET media/photos/main`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "photoUrl":"http://photo.com/1600x900"
    }
]
```

## Загрузка фотографий с игроком

### URL

`GET media/players/{id}/photos`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "photoUrl":"http://photo.com/1600x900"
    }
]
```

## Загрузка типов альбомов с фото

### URL

`GET media/photoalbums/albumTypes`

### Ответ

```json
[
    {
        "id":"0",
        "title":"Все альбомы"
    },
    {
        "id":"1",
        "title":"Матчи"
    }
]
```

## Загрузка списка фото-новостей

### URL

`GET media/photoalbums/?albumTypes={id}`

### Ответ

```json
[
    {
        "id":"photoNews1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```

## Загрузка контента конкретной фото-новости

### URL

`GET media/photoalbums/{id}/`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "photoUrl":"http://photo.com/1600x900"
    },
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "photoUrl":"http://photo.com/1600x900"
    }
]
```