# Описание методов АПИ, связанных с видео

## Загрузка подборки видео на главной

### URL

`GET media/videos/main`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "videoUrl":"http://video.com/1"
    }
]
```

## Загрузка видео с игроком

### URL

`GET media/players/{id}/videos`

### Ответ

```json
[
    {
        "id":"videoNews1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```

## Загрузка чемпионатов, для которых есть видео

### URL

`GET media/videos/filters`

### Ответ

```json
[
    {
        "id":"123",
        "title":"2017/2018",
    },
    {
        "id":"123",
        "title":"2016/2017"
    }
]
```

## Загрузка типов альбомов с видео

### URL

`GET media/videos/albumtypes`

### Ответ

```json
[
    {
        "id":"0",
        "title":"Все альбомы"
    },
    {
        "id":"1",
        "title":"Матчи"
    }
]
```

## Загрузка списка видео в чемпионате и альбоме

### URL

`GET media/videos/?albumTypes={id}&seasonId={id}`

### Ответ

```json
[
    {
        "id":"videoNews1",
        "imageUrl":"http://img.com",
        "badge":
        {
            "text":"ТЕКСТ БЕЙДЖА",
            "color":"FF00FF"
        },
        "title":"Заголовок новости",
        "publicationDate":"YYYY-MM-DDThh:mm:ss",
        "isViewed":false
    }
]
```

## Загрузка контента конкретной видео-новости

### URL

`GET media/videos/{id}/`

### Ответ

```json
[
    {
        "previewPhotoUrl":"http://photo.com/300x300",
        "videoUrl":"http://video.com/1"
    }
]
```