# Описание методов, связанных с разделом "Контакты"

## Загрузка контактов клуба

### URL

`contacts/info`

### Ответ

```json
{
    "office":
    {
        "title":"Офис клуба",
        "address":"197198, пр. Добролюбова,16, корп. 2, лит. А1"
    },
    "fields":
    [
        {
            "title":"Телефон",
            "displayValue":"8 (812) 960 15 80",
            "value":"88129601580",
            "type":0
        },
        {
            "title":"Реклама",
            "displayValue":"reklama@ska.ru",
            "value":"http://reklama@ska.ru",
            "type":1
        },
        {
            "title":"Email",
            "displayValue":"office@ska.ru",
            "value":"office@ska.ru",
            "type":2
        }
    ]
}
```

## Загрузка магазинов для отображения списком

### URL

`GET contacts/shops/?lastId={id}&count=10`

### Ответ

```json
[
    {
        "id":"1",
        "title":"Hockey Club в Пулково",
        "address":"Пулковское ш., 41, лит. 3А, Аэропорт Пулково, Зал вылета",
        "phone":
        {
            "displayValue":"8 (812) 960 15 80",
            "value":"88129601580"
        },
        "workTime":"ежедневно 11.00 — 21.00 (в дни матчей - до 22.00)",
        "coordinates":
        {
            "latitude":56.4543,
            "longitude":45.4545
        }
    }
]
```