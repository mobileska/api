# Описание методов АПИ, связанных с разделом "Настройки"

## Получение признака подписки на пуш-уведомления

### URL

`GET settings/push`

### Ответ

```json
{
    "enabled":true
}
```

## Получение списка событий, на которые можно подписаться для получения пуш-уведомлений

### URL

`GET settings/events`

### Ответ

```json
[
    {
        "groupName":"Статус матча",
        "events":
        [
            {
                "id":"1",
                "title":"Начало периода",
                "enabled":true
            },
            {
                "id":"2",
                "title":"Окончание периода",
                "enabled":false
            }
        ]
    },
    {
        "groupName":"Билеты",
        "events":
        [
            {
                "id":"3",
                "title":"Старт продаж билетов",
                "enabled":true
            },
            {
                "id":"4",
                "title":"Старт продаж абонементов",
                "enabled":false
            }
        ]
    }
]
```

## Отправка изменений подписок на события для получения пуш-уведомлений

### URL

`POST settings/events`

### Запрос

```json
[
    {
        "id":"1",
        "enabled":true
    },
    {
        "id":"2",
        "enabled":true
    }
    {
        "id":"3",
        "enabled":true
    }
    {
        "id":"4",
        "enabled":true
    }
]
```

## Получения информации о подписке на рассылку новостей

### URL

`GET settings/newsSubscribtion`

### Ответ

```json
{
    "pushEnabled":true,
    "newsSubscriber":
    {
        "email":"alexey@gmail.com"
    }
}
```

```json
{
    "pushEnabled":true,
    "newsSubscriber":null
}
```

```json
{
    "pushEnabled":false,
    "newsSubscriber":null
}
```

## Подписка на рассылку новостей

[См. раздел "Новости"](/02_news/README.md)

## Отписка от рассылки новостей

[См. раздел "Новости"](/02_news/README.md)