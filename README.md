# Документация к API мобильного приложения СКА

## Разделы

- [Основные положения](./00_base/README.md)
- [Главная](./01_main/README.md)
- [Новости](./02_news/README.md)
- [Чемпионат](./03_0_championship/README.md)
- [Команда](./04_team/README.md)
- [Клуб](./05_club/README.md)
- [Медиа](./06_media/README.md)
- [Билеты](./07_tickets/README.md)
- [Магазин](./08_shop/README.md)
- [Контакты](./09_contacts/README.md)
- [Световое шоу](./10_lightshow/README.md)
- [Помощь](./11_help/README.md)
- [Настройки](./12_settings/README.md)
